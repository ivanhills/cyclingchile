from flask import Flask, request, jsonify, Response
import json
import mysql.connector
from flask_cors import CORS, cross_origin

app = Flask(__name__)

def getMysqlConnection():
    return mysql.connector.connect(user='testing', host='mysql', port='3306', password='testing', database='test')

@app.route("/")
def hello():
    return "Flask inside Docker,msd,smsd,ms,d!!"

@app.route('/api/conecta', methods=['GET'])
@cross_origin() # allow all origins all methods.
def get_months():
    db = getMysqlConnection()
    output_json="Error!!"
    try:
        sqlstr = "SELECT * from prueba where id=99"
        print(sqlstr)
        cur = db.cursor()
        cur.execute(sqlstr)
        output_json = cur.fetchall()
    except Exception as e:
        print("Error in SQL:\n", e)
    finally:
        db.close()
    return jsonify(result=output_json)

@app.route('/api/muetra', methods=['GET'])
@cross_origin() # allow all origins all methods.
def otro():
    db = getMysqlConnection()
    print(db)
    output_json="Error!!"
    try:
        sqlstr = "SELECT 'jaja','jaja' from prueba"
        print(sqlstr)
        cur = db.cursor()
        cur.execute(sqlstr)
        output_json = cur.fetchall()
    except Exception as e:
        print("Error in SQL:\n", e)
    finally:
        db.close()
    return jsonify(result=output_json)

@app.route('/api/agrega/<nombre>', methods=['GET'])
@cross_origin() # allow all origins all methods.
def agrega(nombre):
    db = getMysqlConnection()
    try:
        sqlstr = "insert into prueba (nombre) values ('"+nombre+"')"
        print(sqlstr)
        cur = db.cursor()
        cur.execute(sqlstr)
        print("Grabado")
        resultado = "Grabado"
    except Exception as e:
        print("Error in SQL:\n", e)
        resultado = "Error"
    finally:
        db.commit()
        db.close()
    return jsonify(result=resultado)
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')